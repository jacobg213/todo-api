<?php

namespace App\Domain\Notes;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NotesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'=> $this->collection->transform(function ($data) {
                return [
                    'id' => $data->id,
                    'title' => $data->title,
                    'body' => $data->body,
                    'type' => $data->type,
                    'created_at' => $data->created_at,
                    'updated_at' => $data->updated_at
                ];
            }),
            'pagination' => [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
        ];
    }
}