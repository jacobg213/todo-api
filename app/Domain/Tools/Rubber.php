<?php

namespace App\Domain\Tools;

use Illuminate\Database\Eloquent\Model;

/**
 * This class doesn't really improve the code either.
 * It is only present to make to code more fun to read.
 */
class Rubber
{
    /**
     * Remove given model from the database.
     * @param Model $model
     */
    public static function erase(Model $model)
    {
        $model->delete();
    }
}
