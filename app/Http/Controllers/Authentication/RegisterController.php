<?php

namespace App\Http\Controllers\Authentication;

use App\Domain\Users\User;
use Illuminate\Http\Request;
use App\Domain\Users\UserResource;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Try to register a new user
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $response['success'] = true;
        $response['message'] = 'New user has been registered.';
        $response['resource'] = new UserResource($user);
        return response()->json($response);
    }
}