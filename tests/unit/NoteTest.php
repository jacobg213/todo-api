<?php

use App\Domain\Notes\Note;
use App\Domain\Tasks\Task;
use App\Domain\Users\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class NoteTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_has_a_title()
    {
        $note = factory(Note::class)->create(['title' => 'First note']);

        $this->assertEquals('First note', $note->title);
    }

    /** @test */
    public function it_has_a_body()
    {
        $note = factory(Note::class)->create(['body' => 'Some text']);

        $this->assertEquals('Some text', $note->body);
    }

    /** @test */
    public function it_has_a_type()
    {
        $note = factory(Note::class)->create(['type' => Note::TYPES['todo']]);

        $this->assertEquals(1, $note->type);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);

        $this->assertEquals($user->id, $note->user->id);
    }

    /** @test */
    public function it_has_many_tasks()
    {
        $note = factory(Note::class)->create();
        factory(Task::class, 3)->create(['note_id' => $note->id]);

        $this->assertEquals(3, $note->tasks()->count());
    }
}
