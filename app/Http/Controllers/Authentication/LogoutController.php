<?php

namespace App\Http\Controllers\Authentication;

use Auth;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    /**
     * Log a user out.
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::logout();

        $response['success'] = true;
        $response['message'] = 'User has been logged out.';

        return response()->json($response);
    }
}