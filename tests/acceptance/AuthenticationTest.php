<?php

use App\Domain\Users\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthenticationTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function new_users_can_be_registered()
    {
        $this->json(
            'POST',
            '/register',
            [
                'name' => 'John Doe',
                'email' => 'jd@example.com',
                'password' => 'strongPassword',
                'password_confirmation' => 'strongPassword'
            ]
        )
        ->seeJson([
            'success' => true,
            'message' => 'New user has been registered.'
        ]);
    }

    /** @test */
    public function a_user_can_log_in_and_out()
    {
        factory(User::class)->create([
            'email' => 'jd@example.com',
            'password' => Hash::make('123456')
        ]);

        $response = $this->call(
            'POST',
            '/login',
            [
                'email' => 'jd@example.com',
                'password' => '123456'
            ]
        );

        $this->assertContains(
            'User has been logged in.', $response->content()
        );

        $token = json_decode($response->content())->token;

        $this->json(
            'POST',
            '/logout',
            [
                'token' => $token
            ]
        )->seeJson([
            'success' => true,
            'message' => 'User has been logged out.'
        ]);
    }
}

