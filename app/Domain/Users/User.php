<?php

namespace App\Domain\Users;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    // Authentication functions
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    // Relations
    public function notes()
    {
        return $this->hasMany('App\Domain\Notes\Note');
    }

    public function tasks()
    {
        return $this->hasManyThrough('App\Domain\Tasks\Task', 'App\Domain\Notes\Note');
    }

    // Authorization
    public function ownership_check($content)
    {
        if(!$content->user_id === $this->id)
        {
            abort(401, 'Unauthorized.');
        }
        return true;
    }
}