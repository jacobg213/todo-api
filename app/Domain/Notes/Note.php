<?php

namespace App\Domain\Notes;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    const TYPES = [
        'regular' => 0,
        'todo' => 1
    ];

    const validationRules = [
        'title' => 'required|max:200',
        'body' => 'max:20000|string|nullable',
        'type' => 'required|in:0,1',
        'tasks' => 'array|nullable',
        'tasks.*.body' => 'string|max:20000',
        'tasks.*.status' => 'boolean|nullable'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'type', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Domain\Users\User');
    }

    public function tasks()
    {
        return $this->hasMany('App\Domain\Tasks\Task');
    }
}