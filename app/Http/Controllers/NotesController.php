<?php

namespace App\Http\Controllers;

use Auth;
use App\Domain\Tools\Pen;
use App\Domain\Notes\Note;
use Illuminate\Http\Request;
use App\Domain\Tools\Rubber;
use App\Domain\Notes\NoteResource;
use App\Domain\Notes\NotesCollection;

class NotesController extends Controller
{
    /**
     * Return a paginated index of user's notes.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json($this->prepare_response(
            true,
            'Returned a paginated index of your notes',
            new NotesCollection(Auth::user()->notes()->paginate(9))
        ));
    }

    /**
     * Return note with given id.
     * Return tasks if any.
     * @param $noteId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($noteId)
    {
        return response()->json($this->prepare_response(
            true,
            "Returned note #{$noteId}.",
            new NoteResource(Note::with('tasks')->find($noteId))
        ));
    }

    /**
     * Try to store a new note in the database.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, Note::validationRules);
        $request['user_id'] = Auth::user()->id;

        $note = Pen::write('App\Domain\Notes\Note', $request->all());

        if((int)$request->type === Note::TYPES['todo'] && count($request->tasks) > 0)
        {
            foreach ($request->tasks as $taskData)
            {
                $taskData['note_id'] = $note->id;
                Pen::write('App\Domain\Tasks\Task', $taskData);
            }
        }

        $response = $this->prepare_response(
            true,
            'New note has been created.',
            new NoteResource($note)
            );
        return response()->json($response);
    }

    /**
     * Try to update the given note.
     * @param $noteId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $noteId)
    {
        $this->validate($request, Note::validationRules);
        $note = Note::find($noteId);
        Auth::user()->ownership_check($note);

        Pen::rewrite($note, $request->all());

        $response = $this->prepare_response(
            true,
            'A note has been updated.',
            new NoteResource($note)
        );
        return response()->json($response);
    }

    /**
     * Erase the given note.
     * @param $noteId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($noteId)
    {
        $note = Note::find($noteId);
        Auth::user()->ownership_check($note);
        Rubber::erase($note);

        return response()->json($this->prepare_response(true, "The note #{$noteId} has been deleted."));
    }
}
