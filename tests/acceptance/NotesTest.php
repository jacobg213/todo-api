<?php

use App\Domain\Notes\Note;
use App\Domain\Tasks\Task;
use App\Domain\Users\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Testing\DatabaseTransactions;

class NotesTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function a_user_can_view_an_index_of_his_notes()
    {
        $user = factory(User::class)->create();
        $notes = factory(Note::class, 3)->create(['user_id' => $user->id]);
        Auth::login($user);

        $response = $this->json(
            'GET',
            '/notes/index'
        );
        $response->seeJson([
            'success' => true,
            'message' => 'Returned a paginated index of your notes',
        ]);
        foreach ($notes as $note)
        {
            $response->seeJson([
                'id' => $note->id,
                'title' => $note->title,
                'body' => $note->body
            ]);
        }
    }

    /** @test */
    public function a_user_can_view_his_entire_note()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        $tasks = factory(Task::class, 3)->create(['note_id' => $note->id]);
        Auth::login($user);

        $response = $this->json(
            'GET',
            '/notes/'.$note->id
        );
        $response->seeJson([
            'success' => true,
            'message' => "Returned note #{$note->id}.",
        ]);
        $response->seeJson([
            'id' => $note->id,
            'title' => $note->title,
            'body' => $note->body,
        ]);
        foreach ($tasks as $task)
        {
            $response->seeJson([
                'id' => $task->id,
                'body' => $task->body
            ]);
        }
    }

    /** @test */
    public function a_user_can_create_new_notes()
    {
        $user = factory(User::class)->create();
        Auth::login($user);

        $this->json(
            'POST',
            '/notes',
            [
                'title' => 'Todo',
                'body' => 'My note description',
                'type' => '1',
                'user_id' => $user->id,
                'tasks' => [
                    0 => [
                        'body' => 'Some task',
                        'status' => false
                    ],
                    1 => [
                        'body' => 'Completed',
                        'status' => true
                    ]
                ]
            ])
            ->seeJson([
                'success' => true,
                'message' => 'New note has been created.'
            ]);

        $newNote = Auth::user()->notes()->firstOrFail();

        $this->seeInDatabase('notes', [
            'title' => 'Todo',
            'body' => 'My note description',
            'type' => 1
        ]);
        $this->seeInDatabase('tasks', [
            'body' => 'Some task',
            'status' => false,
            'note_id' => $newNote->id
        ]);
        $this->seeInDatabase('tasks', [
            'body' => 'Completed',
            'status' => true,
            'note_id' => $newNote->id
        ]);
    }

    /** @test */
    public function a_user_can_edit_his_notes()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        Auth::login($user);

        $this->json(
            'PATCH',
            '/notes/'.$note->id.'/update',
            [
                'title' => 'Todo',
                'body' => 'My note description',
                'type' => '1',
            ])
            ->seeJson([
                'success' => true,
                'message' => 'A note has been updated.'
            ]);

        $this->seeInDatabase('notes', [
            'title' => 'Todo',
            'body' => 'My note description',
            'type' => '1',
        ]);
    }

    /** @test */
    public function a_user_can_delete_his_notes()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        Auth::login($user);

        $this->json(
            'DELETE',
            '/notes/'.$note->id.'/delete'
        )
            ->seeJson([
                'success' => true,
                'message' => "The note #{$note->id} has been deleted."
            ]);

        $this->missingFromDatabase('notes', ['body' => $note->body]);
    }
}
