<?php

namespace App\Domain\Tools;

/**
 * This class doesn't really improve the code.
 * It is only present to make to code more fun to read and work with.
 */
class Pen
{
    /**
     * Create a new model of type $modelClass
     * $modelClass must contain the namespace
     * and the actual name of a class.
     * Model will be created with the $ink values.
     * Remember to validate your $ink!
     * @param $modelClass
     * @param $ink
     * @return mixed
     */
    public static function write($modelClass, $ink)
    {
        return $modelClass::create($ink);
    }


    /**
     * Update given $model with given $ink values.
     * Remember to validate your $ink!
     * @param $model
     * @param $ink
     * @return mixed
     */
    public static function rewrite($model, $ink)
    {
        return $model->update($ink);
    }
}
