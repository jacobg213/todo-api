# TODO demo application API

This is a demo API built with Lumen. Be sure to check out my example [Vue app](https://github.com/jacobg213/vue-todo-example) that uses this as its backend!

## Requirements

All requirements can be found in the [Lumen docs](https://lumen.laravel.com/docs/5.6#server-requirements).

## Installation

* `git clone` the project
* Run `composer install`
* `cp .env.example .env`
* Prepare your DB
* Make sure that everything in your `.env` is correct
* Run `php artisan key:generate`
* Run `php artisan jwt:secret`
* Run `php artisan migrate`

## Testing
* `touch storage/testing.sqlite`
* `php artisan migrate --database test`
* `vendor/bin/phpunit`

## Usage
Accessible by everyone:

* GET `/` - API welcome response.
* POST `/register` with data: (string)`name`, (string)`email`, (string)`password` and (string)`password_confirmation` - registers a new user.
* POST `/login` with data: `email`, `password` - authenticates a user and sends back a token.

Accessible by authenticated users:

* POST `/logout` with data: (string)`token` - logout and invalidate the token.
* GET `/notes/index` with data: (string)`token` - get an index of user's notes.
* GET `/notes/{noteId}` with data: (string)`token` - get a single note with all it's details.
* POST `/notes` with data: (string)`token`, (string)`title`, (int)`type` (in: 0, 1) and optional: (string)`body`, (array)`tasks`, required in tasks array: (string)`body`, optional in tasks array (boolean)`status` (default: false) - creates a new note and it's tasks.
* PATCH `/notes/{noteId}/update` with data: (string)`token`, (string)`title` and (int)`type` (in: 0, 1) - updates a note with given id.
* DELETE `notes/{noteId}/delete` with data: (string)`token` - deletes a note with given id.
* POST `/tasks` with data: (string)`token`, (string)`body`, (int)`note_id` and optional: (boolean)`status` (default: false) - creates a new note.
* PATCH `/tasks/{taskId}/update` with data: (string)`token`, (string)`body`, (int)`note_id` and optional: (boolean)`status` (default: false) - updates a task with given id.
* DELETE `/tasks/{taskId}/delete` with data: (string)`token` - deletes a task with given id.

### Successful request response
Returns json structure:
```
{
    'success': true,
    'message': 'Information about your action' ,
    'token': 'random string' (new token for your next request),
    'resource': data requested (if any)
};
```

### API data reference
Notes:

| Property | Type | Validation |
|:---:|:---:|:---:|
| title | string | required, max 200 characters |
| body | string | nullable, max 20000 characters |
| type | tinyint | required, in 0, 1 |

Tasks:

| Property | Type | Validation |
|:---:|:---:|:---:|
| body | string | required, max 20000 characters |
| status | boolean | nullable |
| note_id | int | required, exists in `notes` table |

Auth:

| Property | Type | Validation |
|:---:|:---:|:---:|
| token | string | required |

### License
Just ask me before taking my stuff.

### Demo
[Check it out here](https://api.todo.gilis.me/)