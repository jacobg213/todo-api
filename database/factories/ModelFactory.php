<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Domain\Notes\Note;
use App\Domain\Tasks\Task;
use App\Domain\Users\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => $faker->password
    ];
});

$factory->define(Note::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->text(200),
        'body' => $faker->paragraph,
        'type' => $faker->numberBetween(0,1),
        'user_id' => factory(User::class)->create()->id
    ];
});

$factory->define(Task::class, function (Faker\Generator $faker) {
    return [
        'body' => $faker->paragraph,
        'status' => $faker->boolean,
        'note_id' => factory(Note::class)->create()->id
    ];
});