<?php

use App\Domain\Notes\Note;
use App\Domain\Tasks\Task;
use App\Domain\Users\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TasksTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function a_user_can_create_new_tasks_in_his_note()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        Auth::login($user);

        $this->json(
            'POST',
            '/tasks',
            [
                'body' => 'My task',
                'status' => false,
                'note_id' => $note->id
            ])
            ->seeJson([
                'success' => true,
                'message' => 'New task has been created.'
            ]);

        $this->seeInDatabase('tasks', [
            'body' => 'My task',
            'status' => false,
            'note_id' => $note->id
        ]);
    }

    /** @test */
    public function a_user_can_edit_his_task()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        $task = factory(Task::class)->create(['note_id' => $note->id]);
        Auth::login($user);

        $this->json(
            'PATCH',
            '/tasks/'.$task->id.'/update',
            [
                'body' => 'My edited task',
                'status' => true,
                'note_id' => $note->id
            ])
            ->seeJson([
                'success' => true,
                'message' => 'A task has been updated.'
            ]);

        $this->seeInDatabase('tasks', [
            'body' => 'My edited task',
            'status' => true,
            'note_id' => $note->id
        ]);
    }

    /** @test */
    public function a_user_can_delete_his_task()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        $task = factory(Task::class)->create(['note_id' => $note->id]);
        Auth::login($user);

        $this->json(
            'DELETE',
            '/tasks/'.$task->id.'/delete'
        )
            ->seeJson([
                'success' => true,
                'message' => "The task #{$task->id} has been deleted."
            ]);

        $this->missingFromDatabase('tasks', ['body' => $task->body]);
    }
}
