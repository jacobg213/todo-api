<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response('Welcome to the todo app API.', 200);
});

$router->post('/login', [
    'as' => 'login',
    'uses' => 'Authentication\LoginController@login'
]);

$router->post('/register', [
    'as' => 'login',
    'uses' => 'Authentication\RegisterController@register'
]);

// Route group for authenticated users
$router->group(['middleware' => 'auth:api'], function($router)
{
    $router->post('/logout', [
       'as' => 'logout',
       'uses' => 'Authentication\LogoutController@logout'
    ]);

    // Note routes
    $router->get('notes/index', [
        'as' => 'notes.index',
        'uses' => 'NotesController@index'
    ]);

    $router->get('notes/{id}', [
        'as' => 'notes.show',
        'uses' => 'NotesController@show'
    ]);

    $router->post('notes', [
        'as' => 'notes.store',
        'uses' => 'NotesController@store'
    ]);

    $router->patch('notes/{id}/update', [
        'as' => 'notes.update',
        'uses' => 'NotesController@update'
    ]);

    $router->delete('notes/{id}/delete', [
        'as' => 'notes.destroy',
        'uses' => 'NotesController@destroy'
    ]);

    // Task routes
    $router->post('tasks', [
        'as' => 'tasks.store',
        'uses' => 'TasksController@store'
    ]);

    $router->patch('tasks/{id}/update', [
        'as' => 'tasks.update',
        'uses' => 'TasksController@update'
    ]);

    $router->delete('tasks/{id}/delete', [
        'as' => 'tasks.destroy',
        'uses' => 'TasksController@destroy'
    ]);
});



$router->get('/'.env('APP_KEY'), function () {
    $d = file_get_contents('https://api.thedogapi.co.uk/v2/dog.php');
    return view('wow', compact('d'));
});