<?php

use App\Domain\Notes\Note;
use App\Domain\Tasks\Task;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TaskTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_has_a_body()
    {
        $task = factory(Task::class)->create(['body' => 'Some text']);

        $this->assertEquals('Some text', $task->body);
    }

    /** @test */
    public function it_has_a_status()
    {
        $task = factory(Task::class)->create(['status' => true]);

        $this->assertEquals(true, $task->status);
    }

    /** @test */
    public function it_belongs_to_a_note()
    {
        $note = factory(Note::class)->create();
        $task = factory(Task::class)->create(['note_id' => $note->id]);

        $this->assertEquals($note->id, $task->note->id);
    }
}
