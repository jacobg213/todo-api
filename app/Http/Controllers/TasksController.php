<?php

namespace App\Http\Controllers;

use Auth;
use App\Domain\Tools\Pen;
use App\Domain\Tasks\Task;
use App\Domain\Notes\Note;
use Illuminate\Http\Request;
use App\Domain\Tools\Rubber;
use App\Domain\Tasks\TaskResource;

class TasksController extends Controller
{
    /**
     * Store a new task
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, Task::validationRules);
        Auth::user()->ownership_check(Note::find($request->note_id));

        $task = Pen::write('App\Domain\Tasks\Task', $request->all());

        $response = $this->prepare_response(
            true,
            'New task has been created.',
            new TaskResource($task)
        );
        return response()->json($response);
    }

    /**
     * Update an existing task
     * @param $taskId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $taskId)
    {
        $this->validate($request, Task::validationRules);
        Auth::user()->ownership_check(Note::find($request->note_id));

        Pen::rewrite($task = Task::find($taskId), $request->all());

        $response = $this->prepare_response(
            true,
            'A task has been updated.',
            new TaskResource($task)
        );
        return response()->json($response);
    }

    /**
     * Delete the given task.
     * @param $taskId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($taskId)
    {
        $task = Task::find($taskId);
        Auth::user()->ownership_check($task->note);
        Rubber::erase($task);

        return response()->json($this->prepare_response(true, "The task #{$taskId} has been deleted."));
    }
}
