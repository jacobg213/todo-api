<?php

namespace App\Http\Controllers;

use Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Prepare a typical API response.
     * @param bool $status
     * @param String $message
     * @param mixed $data
     * @return array
     */
    public function prepare_response($status, String $message, $data = null)
    {
        return [
            'success' => $status,
            'message' => $message,
            'token' => Auth::refresh(),
            'resource' => $data
        ];
    }
}
