<?php

namespace App\Domain\Tasks;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const validationRules = [
            'body' => 'required|string|max:20000',
            'status' => 'boolean|nullable',
            'note_id' => 'required|exists:notes,id'
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body', 'status', 'note_id'
    ];

    public function note()
    {
        return $this->belongsTo('App\Domain\Notes\Note');
    }
}