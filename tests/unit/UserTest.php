<?php

use App\Domain\Notes\Note;
use App\Domain\Tasks\Task;
use App\Domain\Users\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_has_a_name()
    {
        $user = factory(User::class)->create(['name' => 'John Doe']);

        $this->assertEquals('John Doe', $user->name);
    }

    /** @test */
    public function it_has_an_email()
    {
        $user = factory(User::class)->create(['email' => 'jd@example.com']);

        $this->assertEquals('jd@example.com', $user->email);
    }

    /** @test */
    public function it_has_a_password()
    {
        $user = factory(User::class)->create(['password' => Hash::make('strongPassword')]);

        $this->assertTrue(Hash::check('strongPassword', $user->password));
    }

    /** @test */
    public function it_has_many_notes()
    {
        $user = factory(User::class)->create();
        factory(Note::class, 3)->create(['user_id' => $user->id]);

        $this->assertEquals(3, $user->notes()->count());
    }

    /** @test */
    public function it_has_many_tasks_through_notes()
    {
        $user = factory(User::class)->create();
        $note = factory(Note::class)->create(['user_id' => $user->id]);
        factory(Task::class, 3)->create(['note_id' => $note->id]);

        $this->assertEquals(3, $user->tasks()->count());
    }
}
