<?php

class HomeTest extends TestCase
{
    /** @test */
    public function the_root_uri_can_be_accessed_by_everyone()
    {
        $this->get('/');

        $this->assertEquals(
            'Welcome to the todo app API.', $this->response->getContent()
        );
    }
}
